<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API\Book')->middleware('auth:api')->group(function () {
    Route::resource('books', 'BookController');
});

Route::namespace('API\Auth')->prefix('auth')->group(function() {
    Route::post('/login', 'AuthController@login');

    Route::middleware('auth:api')->group(function () {
        Route::get('/user', 'AuthController@user');
        Route::get('/logout', 'AuthController@logout');
    });

    Route::post('/register', 'AuthController@register');
    Route::get('/register/activate/{token}', 'AuthController@activateRegister');
    Route::get('/register/resend-confirmation', 'AuthController@resendActivationEmail');

    Route::post('/password', 'PasswordResetController@create');
    Route::get('/password/{token}', 'PasswordResetController@find');
    Route::post('/password/reset', 'PasswordResetController@reset');
});
