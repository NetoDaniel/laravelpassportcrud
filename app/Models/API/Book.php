<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'name', 'author'
    ];
}
