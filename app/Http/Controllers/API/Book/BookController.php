<?php

namespace App\Http\Controllers\API\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\API\Book;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{

    public function index()
    {
        $books = Book::all();

        return response()->json([
            'success' => true,
            'data' => $books->toArray(),
            'message' => 'Livros recuperados com sucesso.'
        ], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
            'author' => ['required', 'string', 'max:191']
        ]);

        if ($validator->fails()) {

            return response()->json([
                'success' => false,
                'data' => $validator->errors(),
                'message' => 'Erro de validação dos campos.'
            ], 400);
        }

        $book = Book::create($data);

        return response()->json([
            'success' => true,
            'data' => $book->toArray(),
            'message' => 'Livro cadastrado com sucesso.'
        ], 201);
    }

    public function show($id)
    {
        $book = Book::find($id);

        if (is_null($book)) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'Livro não encontrado.'
            ], 204);
        }

        return response()->json([
            'success' => true,
            'data' => $book->toArray(),
            'message' => 'Livro encontrado com sucesso.'
        ], 200);
    }

    public function update(Request $request, Book $book)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
            'author' => ['required', 'string', 'max:191']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors(),
                'message' => 'Erro de validação dos campos.'
            ], 400);
        }

        $book->name = $data['name'];
        $book->author = $data['author'];
        $book->save();

        return response()->json([
            'success' => true,
            'data' => $book->toArray(),
            'message' => 'Livro atualizado com sucesso'
        ], 201);
    }

    public function destroy(Book $book)
    {
        $book->delete();

        return response()->json([
            'success' => true,
            'data' => $book->toArray(),
            'message' => 'Livro deletado com sucesso'
        ], 200);
    }
}
