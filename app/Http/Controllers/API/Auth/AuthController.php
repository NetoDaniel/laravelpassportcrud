<?php

namespace App\Http\Controllers\API\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SingUpNotification;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors(),
                'message' => 'Erro de validação dos campos.'
            ], 400);
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'activation_token' => Str::random(60)
        ]);

        $user->notify(new SingUpNotification($user));

        return response()->json([
            'success' => true,
            'message' => 'Cadastro realizado com sucesso! Por favor acesse seu e-mail e confirme sua conta.'
        ], 201);
    }

    public function activateRegister($token)
    {
        $user = User::where('activation_token', $token)->first();

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => 'Token de ativação inválido!'
            ], 404);
        }

        $user->active = true;
        $user->activation_token = '';
        $user->save();

        $token = $user->createToken('authtoken')->accessToken;

        return response()->json([
            'success' => true,
            'token' => $token,
            'message' => 'Cadastro ativado com sucesso.'
        ], 200);
    }

    public function resendActivationEmail(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors(),
                'message' => 'Erro de validação dos campos.'
            ], 400);
        }

        $user = User::where('email', $request->email)->where('active', false)->first();

        $user->notify(new SingUpNotification($user));

        return response()->json([
            'success' => true,
            'message' => 'Confirmação reenviada com sucesso! Acesse seu e-mail e ative o cadastro.'
        ], 200);
    }

    public function user(Request $request)
    {
        return response()->json([
            'success' => true,
            'data' => $request->user()->toArray(),
            'message' => 'Usuario recuperado com sucesso.'
        ], 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $credentails['deleted_at'] = null;

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            if ($user->active) {
                $token = $user->createToken('authtoken')->accessToken;

                return response()->json([
                    'success' => true,
                    'token' => $token,
                    'message' => 'Usuário autenticado com sucesso.'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Cadastro não confirmado.',
                    'action' => [
                        'desc' => 'Reenviar e-mail de confirmação.',
                        'uri' => env('APP_URL', 'http://localhost') . '/api/auth/register/resend-confirmation',
                        'method' => 'GET'
                    ]
                ], 401);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Acesso não autorizado.'
            ], 401);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'success' => true,
            'message' => 'Usuário desconectado com sucesso'
        ], 200);
    }
}
