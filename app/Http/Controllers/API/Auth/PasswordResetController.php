<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\User;
use App\PasswordReset;
use App\Notifications\PassResetRequestNotification;
use App\Notifications\PassResetSuccessNotification;
use Carbon\Carbon;

class PasswordResetController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors(),
                'message' => 'Erro de validação dos campos.'
            ], 400);
        }

        $user = User::where('email', $request->email)->where('active', true)->first();

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => 'E-mail não encontrado.'
            ], 404);
        }

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60)
            ]
        );

        if ($user && $passwordReset) {
            $user->notify(new PassResetRequestNotification($passwordReset->token));
        }

        return response()->json([
            'success' => true,
            'message' => 'Recuperação de senha enviada com sucesso.'
        ]);
    }

    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (is_null($passwordReset)) {
            return response()->json([
                'success' => false,
                'message' => 'Token de recuperação de senha inválido.'
            ], 404);
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();

            return response()->json([
                'success' => false,
                'message' => 'Token de recuperação de senha expirado.'
            ], 404);
        }

        return response()->json([
            'success' => true,
            'data' => $passwordReset,
        ]);
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string', 'confirmed'],
            'token' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors(),
                'message' => 'Erro de validação.'
            ], 400);
        }

        $passwordReset = PasswordReset::where('token', $request->token)->where('email', $request->email)->first();

        if (is_null($passwordReset)) {
            return response()->json([
                'success' => false,
                'message' => 'Token de recuperação de senha inválido.'
            ], 404);
        }

        $user = User::where('email', $passwordReset->email)->first();

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => 'E-mail não encontrado.'
            ], 404);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        $passwordReset->delete();

        $user->notify(new PassResetSuccessNotification($passwordReset));

        return response()->json([
            'success' => true,
            'message' => 'Senha recuperada com sucesso.'
        ]);
    }
}
